package main

import (
	"bytes"
	"encoding/xml"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)


type DataSet struct {
	XMLName xml.Name `xml:"dataset"`
	Record  []struct {
		ID        string `xml:"id"`
		FirstName string `xml:"first_name"`
		LastName  string `xml:"last_name"`
		Email     string `xml:"email"`
	} `xml:"record"`
} 



// Function to push the extracted fields to Solr
func pushToSolr(records string) error {

	// Create a new HTTP request to add the document to Solr
	req, err := http.NewRequest("POST", "http://localhost:8983/solr/dataset/update?commit=true", bytes.NewBufferString(records))
	if err != nil {
		return err
	}

	// Set the request headers
	req.Header.Set("Content-Type", "application/json")

	// Send the request to Solr
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	// Check the response status code
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to push document to Solr, status code: %d", resp.StatusCode)
	}

	// Close the response body
	defer resp.Body.Close()

	return nil
}

func main() {
	// Specify the XML file path
	xmlPath := "data/dataset.xml"

	// Open the XML file
	xmlFile, err := os.Open(xmlPath)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer xmlFile.Close()

	// Create an XML decoder
	xmlDecoder := xml.NewDecoder(xmlFile)

	// Parse the XML file
	var data DataSet
	err = xmlDecoder.Decode(&data)
	if err != nil {
		fmt.Println("Error parsing XML:", err)
		return
	}

	docs, err := json.Marshal(data.Record)
		if err != nil {
		fmt.Println("Error converting XML to JSON:", err)
		return
	}

	// Push the extracted fields to Solr
	err = pushToSolr(string(docs))
	if err != nil {
		fmt.Println("Error pushing to Solr:", err)
		return
	}

	fmt.Println("Fields pushed to Solr successfully.")
}

