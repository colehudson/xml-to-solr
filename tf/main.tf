terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.2"
    }
  }
}

# Adjust to your local docker endpoint. `docker context ls` might yield the appropriate endpoint
provider "docker" {
  host = ""
}

# Uses solr 9.5 nightly build due to bug when running custom commands off Solr docker images: https://issues.apache.org/jira/browse/SOLR-17039
resource "docker_image" "solr" {
  name         = "apache/solr-nightly:9.5.0-SNAPSHOT-slim"
  keep_locally = false

}

resource "docker_container" "solr" {
  image = docker_image.solr.image_id
  name  = "my_solr"

  ports {
    internal = 8983
    external = 8983
  }

  # Command to run Solr and create the core
  command = [
    "/bin/bash",
    "-c",
    "solr start && solr create_core -c dataset && tail -f /dev/null"
  ]

}