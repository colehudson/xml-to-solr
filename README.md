# Xml to Solr

Personal project to write more in Go. Takes a provided sample XML, maps it a struct, and converts to JSON before pushing to Solr. It presumes a Solr environment as mentioned below.


## Getting started

I usually start a Solr container for quick use - either using the provided terraform file in the tf directory (adding in appropriate variables where needed) or running the below commands manually.

`docker run -d -p 8983:8983 --name my_solr solr`

`docker exec -it my_solr solr create_core -c dataset`

Then `go run xmlToSolr.go`
